package com.company.dia104.Geometria;

public interface Calculadora {
    public double calcArea(Circulo circulo);
    public double calcArea(Retangulo retangulo);
    public double calcArea(Triangulo triangulo);
}
