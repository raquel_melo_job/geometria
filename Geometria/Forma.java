package com.company.dia104.Geometria;

public abstract class Forma {

    private int quantLados;

    public Forma(int quantLados) {
        this.quantLados = quantLados;
    }

    public int getQuantLados() {
        return quantLados;
    }

    public void setQuantLados(int quantLados) {
        this.quantLados = quantLados;
    }
}
