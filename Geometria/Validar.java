package com.company.dia104.Geometria;

public class Validar {

    private Calculo calc;
    private IO entrada;

    public Validar() {
        this.entrada = new IO();
    }


    public boolean validaTriangulo(Triangulo triangulo){
        double ladoa = triangulo.getLadoa();
        double ladob = triangulo.getLadob();
        double ladoc = triangulo.getLadoc();
        if (
                (ladoa + ladob > ladoc) &&
                        (ladoa + ladoc > ladob) &&
                        (ladob + ladoc > ladoa)
        ){
            return true;
        }else{
            entrada.trianguloInvalido();
            return false;
        }
    }
}
