package com.company.dia104.Geometria;

public class Triangulo extends Forma{

    private double ladoa;
    private double ladob;
    private double ladoc;

    public Triangulo(double ladoa, double ladob, double ladoc, int quantLados) {
        super(quantLados);
        this.ladoa = ladoa;
        this.ladob = ladob;
        this.ladoc = ladoc;
    }

    public double getLadoa() {
        return ladoa;
    }

    public void setLadoa(double ladoa) {
        this.ladoa = ladoa;
    }

    public double getLadob() {
        return ladob;
    }

    public void setLadob(double ladob) {
        this.ladob = ladob;
    }

    public double getLadoc() {
        return ladoc;
    }

    public void setLadoc(double ladoc) {
        this.ladoc = ladoc;
    }
}
