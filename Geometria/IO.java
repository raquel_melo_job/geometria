package com.company.dia104.Geometria;

import java.util.Scanner;

public class IO {

    private Scanner in;

    public IO() {
        this.in = new Scanner(System.in);
    }

    public int perguntaFormato(){
        System.out.print("Qual forma geométrica deseja criar? " +
                "Digite: \n 1 para Circulo \n 3 para Triangulo \n " +
                "4 para Retangulo : ");
        return in.nextInt();
    }

    public void retornaArea(Double area){
        System.out.println("A área do formato geométrico é: "+area);
    }

    public double[] medidasRetangulo(){
        double[] medidas = new double[2];
        System.out.print("Qual é a altura do retângulo: ");
        medidas[0] = in.nextDouble();
        System.out.print("Qual é a largura do retângulo: ");
        medidas[1] = in.nextDouble();
        return medidas;
    }
    public double[] medidasTriangulo(){
        double[] medidas = new double[3];
        System.out.print("Entre com o lado A: ");
        medidas[0] = in.nextDouble();
        System.out.print("Entre com o lado B: ");
        medidas[1] = in.nextDouble();
        System.out.print("Entre com o lado C: ");
        medidas[2] = in.nextDouble();
        return medidas;
    }
    public double medidaCirculo(){
        System.out.print("Qual é o raio do circulo? ");
        return in.nextDouble();
    }

    public void trianguloInvalido(){
        System.out.println("O triangulo não é válido.");
    }

    public int saidaPrograma(){
        System.out.println("Pressione 1 para continuar ou 2 para sair");
        return in.nextInt();
    }

    public void quantLadosInvalido() {
        System.out.println("Quantidade de lados inválida");
    }
}
