package com.company.dia104.Geometria;

public class Circulo extends Forma {
    private double raio;

    public Circulo(double raio, int quantLados) {
        super(quantLados);
        this.raio = raio;
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }
}
