package com.company.dia104.Geometria;


public class Calculo implements Calculadora {

    IO entrada;
    Validar validacao;

    public Calculo() {
        this.entrada = new IO();
        this.validacao = new Validar();
    }

    @Override
    public double calcArea(Circulo circulo) {
        return Math.pow(circulo.getRaio(), 2) * Math.PI;
    }

    @Override
    public double calcArea(Retangulo retangulo) {
        return  retangulo.getAltura() * retangulo.getLargura();
    }

    @Override
    public double calcArea(Triangulo triangulo) {
        double area = 0;
        boolean isTrianguloValido = validacao.validaTriangulo(triangulo);
        if(isTrianguloValido){
            double s = (triangulo.getLadoa() + triangulo.getLadob() + triangulo.getLadoc() / 2);
            area = Math.sqrt(s * (s - triangulo.getLadoa()) * (s - triangulo.getLadob()) * (s - triangulo.getLadoc()));
        }
        return area;
    }
}