package com.company.dia104.Geometria;

import org.w3c.dom.css.CSSImportRule;

public class FormasGeometricas {

    //Programa que inicia o projeto
    IO entrada;
    Validar validacao;
    Circulo circulo;
    Triangulo triangulo;
    Retangulo retangulo;
    Calculo calculo;

    public FormasGeometricas() {
        this.entrada = new IO();
        this.validacao = new Validar();
        this.calculo = new Calculo();
    }

    public void inicio(){
        boolean flag = true;
        do{
            flag = this.execucao();
            int continuar = entrada.saidaPrograma();
            if (continuar==2){
                flag = false;
            }

        }while(flag);
    }

    public boolean execucao(){
        int quantLados = this.entrada.perguntaFormato();
        double area = 0;
        if (quantLados == 1){
            area = this.criaCirculo();
        }else if(quantLados == 3){
            area = this.criaTriangulo();
        }else if(quantLados == 4){
            area = this.criaRetangulo();
        }else {
            entrada.quantLadosInvalido();
        }
        if (area!=0){
            entrada.retornaArea(area);
        }

        return true;
    }

    private double criaRetangulo() {
        double[] valores = entrada.medidasRetangulo();
        double altura = valores[0];
        double largura = valores[1];
        retangulo = new Retangulo(altura,largura,4);
        return calculo.calcArea(retangulo);
    }

    private double criaTriangulo() {
        double[] valores = entrada.medidasTriangulo();
        double ladoA = valores[0];
        double ladoB = valores[1];
        double ladoC = valores[2];
        triangulo = new Triangulo(ladoA,ladoB,ladoC,3);
        return calculo.calcArea(triangulo);
    }

    private double criaCirculo() {
        circulo = new Circulo(entrada.medidaCirculo(),1);
        return calculo.calcArea(circulo);
    }


}
